package service;

import dto.ProductDto;
import dto.ProductResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductService {
    private final static Logger log = LoggerFactory.getLogger(ProductService.class);

    public List<ProductResultDto> convertAndCollectProducts(List<String> products,
                                                            Map<String, ProductDto> productConvertMap) {
        if (products == null || productConvertMap == null) {
            log.error("Products list or converted map is null: products {}, productConvertMap {}", products, productConvertMap);
            return new ArrayList<>();

        }
        log.info("Starts conversion with size: {}, at: {}", products.size(), LocalTime.now());
        Map<String, Long> productsQuantity = products.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        List<ProductResultDto> convertedProducts = productsQuantity.entrySet().stream()
                .map(x -> {
                    ProductDto productDto = productConvertMap.get(x.getKey());
                    if (productDto == null) {
                        log.error("Can't find mapping for product: {}", x.getKey());
                        return Optional.<ProductResultDto>empty();
                    }
                    return Optional.of(new ProductResultDto(productDto.version(), productDto.edition(), x.getValue()));
                })
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        log.info("Conversion ends successfully with size: {}, at: {}", convertedProducts.size(), LocalTime.now());
        return convertedProducts;
    }
}
