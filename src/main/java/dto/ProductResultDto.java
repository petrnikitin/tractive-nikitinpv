package dto;

public record ProductResultDto(Integer version, String edition, Long quantity) {

    @Override
    public String toString() {
        return "{" +
                "version=" + version +
                ", edition='" + edition + '\'' +
                ", quantity=" + quantity +
                '}';
    }

}
