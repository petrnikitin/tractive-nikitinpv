package dto;

public record ProductDto(Integer version, String edition) {

}
