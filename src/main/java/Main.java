import dto.ProductDto;
import org.apache.log4j.BasicConfigurator;
import service.ProductService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        BasicConfigurator.configure();
        List<String> products = Arrays.asList("CVCD", "SDFD", "DDDF", "SDFD");


        Map<String, ProductDto> mappings = Map.of(
                "CVCD", new ProductDto(1, "X"),
                "SDFD", new ProductDto(2, "Z"),
                "DDDF", new ProductDto(1, null)
        );
        ProductService productService = new ProductService();
        System.out.println(productService.convertAndCollectProducts(products, mappings));
    }
}
