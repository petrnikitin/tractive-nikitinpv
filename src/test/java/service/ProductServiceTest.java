package service;

import dto.ProductDto;
import dto.ProductResultDto;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProductServiceTest {
    private final ProductService productService = new ProductService();

    @Before
    public void setUp() {
        BasicConfigurator.configure();
    }

    @Test
    public void convertAndCollectProductsSuccessfulTest() {
        List<String> products = Arrays.asList("CVCD", "SDFD", "DDDF", "SDFD");
        Map<String, ProductDto> mappings = Map.of(
                "CVCD", new ProductDto(1, "X"),
                "SDFD", new ProductDto(2, "Z"),
                "DDDF", new ProductDto(1, null)
        );

        List<ProductResultDto> productResults = productService.convertAndCollectProducts(products, mappings);

        assertEquals("Wrong quantity of aggregated products in the list", 3, productResults.size());
        List<ProductResultDto> expectedProducts = List.of(
                new ProductResultDto(2, "Z", 2L),
                new ProductResultDto(1, "X", 1L),
                new ProductResultDto(1, null, 1L)
        );
        assertTrue(expectedProducts.containsAll(productResults) && productResults.containsAll(expectedProducts));
    }

    @Test
    public void convertAndCollectProductsSkipProductsWithoutMappingTest() {
        List<String> products = Arrays.asList("CVCD", "XXXX", "YYYY");
        Map<String, ProductDto> mappings = Map.of(
                "CVCD", new ProductDto(1, "X")
        );

        List<ProductResultDto> productResults = productService.convertAndCollectProducts(products, mappings);

        assertEquals("Wrong quantity of aggregated products in the list", 1, productResults.size());
        List<ProductResultDto> expectedProducts = List.of(
                new ProductResultDto(1, "X", 1L)
        );
        assertTrue(expectedProducts.containsAll(productResults) && productResults.containsAll(expectedProducts));
    }
}