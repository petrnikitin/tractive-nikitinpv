### tractive-nikitinpv
#### Description:

There is only one method that converts codes of products with map
of mappings for product codes into the aggregated list of purchased
products and quantity.

**Process Description**
1. Method takes two inputs (products and map of mappings)
2. Calculation of the quantity of each group of products
3. Forming a response and filtering out product codes that are not included in the mapping
4. Logging of each incorrect code product
5. Displaying a message in console
